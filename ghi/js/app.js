function createCard(name, description, pictureUrl, formattedStart, formattedEnd, location) {
    return `
      <div class="col">
        <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtit;e mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">${formattedStart}-${formattedEnd}</div>
        </div>
      </div>
    `;
  }

function alert(response) {
    return `
        <div class="alert alert-warning" role="alert">
        Could not load conferences
        </div>
    `;
}

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';
//     try {
//         const response = await fetch(url);

//         if (!response.ok) {
//           // Figure out what to do when the response is bad

//         } else {
//           const data = await response.json();

//           const conference = data.conferences[0];
//           const nameTag = document.querySelector('.card-title');
//           nameTag.innerHTML = conference.name;

//           const detailUrl = `http://localhost:8000${conference.href}`;
//           const detailResponse = await fetch(detailUrl);
//           if (detailResponse.ok) {
//             const details = await detailResponse.json();

//             const description = details.conference.description;
//             const descriptionTag = document.querySelector('.card-text');
//             descriptionTag.innerHTML = description;

//             const imageTag = document.querySelector('.card-img-top');
//             imageTag.src = details.conference.location.picture_url;
//           }

//         }
//       } catch (e) {
//         // Figure out what to do if an error is raised
//       }


// });

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const errorMessage = alert(response);
        const column = document.querySelector('.row');
        column.innerHTML += errorMessage;

      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name

            const startDate = details.conference.starts;
            const startDateObj = new Date(startDate);
            const formattedStart = startDateObj.toLocaleDateString();

            const endDate = details.conference.ends;
            const endDateObj = new Date(endDate)
            const formattedEnd = endDateObj.toLocaleDateString();

            const html = createCard(title, description, pictureUrl, formattedStart, formattedEnd, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error(e);
    }

  });
